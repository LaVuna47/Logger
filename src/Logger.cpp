//
// Created by ivan on 15.02.22.
//

#include "Logger.h"

// boost
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/logger.hpp>
#include <utility>
#include <boost/move/utility_core.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared_object.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/core/null_deleter.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/phoenix/bind.hpp>

// std
#include <fstream>
#include <ostream>
#include <boost/log/attributes/scoped_attribute.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

using namespace my::log;


BOOST_LOG_ATTRIBUTE_KEYWORD(timestamp, "TimeStamp", boost::log::attributes::utc_clock::value_type)
BOOST_LOG_ATTRIBUTE_KEYWORD(thread_id, "ThreadID", boost::log::attributes::current_thread_id::value_type)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", eSeverityLevel)
BOOST_LOG_ATTRIBUTE_KEYWORD(process_id, "ProcessID", boost::log::attributes::current_process_id::value_type)
BOOST_LOG_ATTRIBUTE_KEYWORD(line_id, "LineID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(tag_attr, "Tag", std::string)
BOOST_LOG_ATTRIBUTE_KEYWORD(scope, "Scope", attrs::named_scope::value_type)

namespace my::log
{
	namespace impl
	{
		logging::formatter GetFormatter();

		void AddAttributes();

		void AddSinks(const ConfigurationList &cfg);

		bool my_filter(logging::value_ref<eSeverityLevel, tag::severity> const &level,
					   logging::value_ref<std::string, tag::tag_attr> const &tag);
	}

	std::ostream & operator<<(std::ostream & strm, eSeverityLevel level);

}


/*
 * =============== GLOBAL ===============
 */
typedef sinks::synchronous_sink< sinks::text_file_backend > sink_t;
typedef sinks::synchronous_sink< sinks::text_ostream_backend > text_sink;

static src::severity_logger< my::log::eSeverityLevel > g_severityLogger;
static boost::shared_ptr<sink_t> g_FileSink;
static boost::shared_ptr< text_sink > g_ConsoleSink;
static std::optional<eSeverityLevel> g_SeverityFilter;



/*
 * ============== IMPL ===============
 */

std::ostream & my::log::operator<<(std::ostream & strm, eSeverityLevel level)
{
	const char* str;
	switch (level)
	{
		case eSeverityLevel::trace: str = "TRACE"; break;
		case eSeverityLevel::debug: str = "DEBUG"; break;
		case eSeverityLevel::info: str = "INFO"; break;
		case eSeverityLevel::warning: str = "WARNING"; break;
		case eSeverityLevel::error: str = "ERROR"; break;
		case eSeverityLevel::fatal: str = "FATAL"; break;
	}
	strm << str;
	return strm;
}

void my::log::impl::AddAttributes()
{
	auto core = boost::log::core::get();

	// Add commonly used attributes, such as timestamp, thread id, etc.
	core->add_global_attribute("TimeStamp", boost::log::attributes::utc_clock());
	core->add_global_attribute("ThreadID", boost::log::attributes::current_thread_id());
	core->add_global_attribute("ProcessID", boost::log::attributes::current_process_id());
	core->add_global_attribute("LineID", attrs::counter< unsigned int >(1));
	core->add_global_attribute("Scope", attrs::named_scope());

}

void my::log::impl::AddSinks(const ConfigurationList &cfg)
{
	/// TODO: specify here symlink on actual filename.
	/// 	Create filename that is bigger by 1 from previous filename ctr
	auto core = boost::log::core::get();

	// Construct the sink

	auto backend=boost::make_shared< sinks::text_file_backend >(
					keywords::file_name = cfg.logPath,
					keywords::rotation_size = cfg.rotationSize);

	g_FileSink = boost::make_shared< sink_t >(backend);

	// Add file sink
	logging::formatter formatter = impl::GetFormatter();
	// Set formatter
	g_FileSink->set_formatter(formatter);

	// Add the sink to core
	core->add_sink(g_FileSink);

	if(cfg.consoleOutputEnabled)
	{
		g_ConsoleSink = boost::make_shared<text_sink>();
		g_ConsoleSink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()));
		// Set formatter
		g_ConsoleSink->set_formatter(formatter);
		// Add the sink to core
		core->add_sink(g_ConsoleSink);
	}
}

logging::formatter my::log::impl::GetFormatter()
{
	logging::formatter fmt = expr::stream
			<< std::setw(6) << std::setfill('0') << line_id << std::setfill(' ') << ": "
			<< "[" << boost::log::expressions::format_date_time(timestamp,  "%Y-%M-%d %H:%M:%S.%f") << "]:"
			<< "[" << process_id << "]:"
			<< "[" << thread_id << "]: "
			<< severity << " "
			<< expr::if_(expr::has_attr(tag_attr)) [expr::stream << "<" << tag_attr << ">:"]
			<< expr::if_(expr::has_attr(scope)) [expr::stream << " " << scope << " "]
			<< "{" << expr::smessage << "}";
	return fmt;
}




void my::log::StartupLogger(const ConfigurationList &cfg)
{
	impl::AddAttributes();
	impl::AddSinks(cfg);
}

void my::log::LogMessage(eSeverityLevel lvl, const std::string& msg, const std::string& tag_str)
{
	if(!tag_str.empty())
	{
		BOOST_LOG_SCOPED_THREAD_TAG("Tag", tag_str);
		BOOST_LOG_SEV(g_severityLogger, lvl) << msg;
	}
	else
	{
		BOOST_LOG_SEV(g_severityLogger, lvl) << msg;
	}
}

bool my::log::impl::my_filter(logging::value_ref< eSeverityLevel, tag::severity > const& level,
               logging::value_ref< std::string, tag::tag_attr > const& tag)
{
	if(g_SeverityFilter.has_value())
	{
		return level >= g_SeverityFilter.value();
//		return level >= g_SeverityFilter.value() || tag == "IMPORTANT_MESSAGE";
	}
	return false;
}

void my::log::SetFilter(eSeverityLevel severityLvl)
{
	g_SeverityFilter = severityLvl;
	g_FileSink->set_filter(boost::phoenix::bind(&my::log::impl::my_filter, severity_type::or_none(), tag_attr_type::or_none()));
	g_ConsoleSink->set_filter(boost::phoenix::bind(&my::log::impl::my_filter, severity_type::or_none(), tag_attr_type::or_none()));
}

void my::log::ResetFilter()
{
	g_FileSink->reset_filter();
	g_ConsoleSink->reset_filter();
}

my::log::ConfigurationList my::log::ConstructConfigList(
		bool consoleOutputEnabled,
		std::filesystem::path logPathStdOut,
		uint64_t rotationSize)
{
	ConfigurationList list;
	list.logPath = std::move(logPathStdOut);
	list.consoleOutputEnabled = consoleOutputEnabled;
	list.rotationSize = rotationSize;
	return list;
}