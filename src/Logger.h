//
// Created by ivan on 15.02.22.
//

#ifndef LOGGER_LOGGER_H
#define LOGGER_LOGGER_H

#include <string>
#include <filesystem>
#include <optional>
#include <vector>
#include <functional>
#include <boost/log/attributes.hpp>
#include <boost/log/attributes/scoped_attribute.hpp>

#define DEFAULT_ROTATION_SIZE 1024 * 1024 * 6 // 6 MB

/**
 * my - namespace for 'my' libraries, code, applications.
 * In general anything can be added into 'my' namespace, but tools are preferable.
 */
namespace my::log
{

	enum eSeverityLevel : uint32_t
	{
		/* Use to "trace" the code when trying to find one part of a function specifically. */
		trace = 0,
		/* Information that is diagnostically helpful to people more than just developers (IT, sysadmins, etc.) */
		debug = 10,
		/* Generally useful information to log (service start/stop, configuration assumptions, etc).
		 * Info I want to always have available but usually don't care about under normal circumstances.*/
		info = 20,
		/*  Anything that can potentially cause application oddities, but for which I am automatically recovering.
		 * (Such as switching from a primary to back up server, retrying an operation, missing secondary data, etc.)
		 * */
		warning = 40,
		/*
		 * Any error which is fatal to the operation, but not the service or application
		 * (can't open a required file, missing data, etc.). These errors will force user
		 * (administrator, or direct user) intervention. These are usually reserved (in my apps)
		 * for incorrect connection strings, missing services, etc.
		 */
		error = 60,
		/*
		 * Any error that is forcing a shutdown of the service or application to prevent
		 * data loss (or further data loss). I reserve these only for the most heinous errors and
		 * situations where there is guaranteed to have been data corruption or loss.
		 */
		fatal = 100
	};

	struct ConfigurationList
	{
		/* Specifies path of the file where content will be logged. */
		std::filesystem::path logPath;
		/* if true - logger will also output to stdout */
		bool consoleOutputEnabled;
		uint64_t rotationSize;
	};

	/**
	 * This class will be used for class specific logging
	 */
    class ILogger
    {
		// TODO: implement class specific logger
    };


	void StartupLogger(const ConfigurationList &config);

	/**
	 * Sets filter for logging
	 * @param severity: all levels that are >= that given severity will be logged
	 */
	void SetFilter(eSeverityLevel severity);

	/**
	 * Clears the filter set by my::log::core::SetFilter function
	 */
	void ResetFilter();

	/**
	 * Releases the resources
	 */
	[[maybe_unused]] void ShutDownLogger();

	void LogMessage(eSeverityLevel lvl, const std::string& msg, const std::string& tag_str = {});


	ConfigurationList ConstructConfigList(
			bool consoleOutputEnabled = false,
			std::filesystem::path logPathStdOut = "run/logs/execution_%N.log",
			uint64_t rotationSize = DEFAULT_ROTATION_SIZE);


}


/// Macros for ILogger using

#define LOG_TRACE(msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::trace, ss.str()); \
}


#define LOG_DEBUG(msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::debug, ss.str()); \
}

#define LOG_INFO(msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::info, ss.str()); \
}

#define LOG_WARNING(msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::warning, ss.str()); \
}

#define LOG_ERROR(msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::error, ss.str()); \
}

#define LOG_FATAL(msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::fatal, ss.str()); \
}

#define LOG_WITH_TAG(sevLvl, tag, msg) \
{                      \
	std::stringstream ss; \
	ss << msg; \
	my::log::LogMessage(my::log::eSeverityLevel::trace, ss.str(), tag); \
}

#define LOG_NAMED_SCOPE(scope_str) \
	BOOST_LOG_NAMED_SCOPE(scope_str) \

#endif // LOGGER_LOGGER_H
