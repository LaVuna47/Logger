#include "Logger.h"
#include <cstddef>
#include <string>
#include <ostream>


int main(int, char*[])
{

	using namespace my::log;
	StartupLogger(my::log::ConstructConfigList(true));
	SetFilter(warning);
	ResetFilter();
	LOG_TRACE("Trace message" << "--")
	LOG_DEBUG("DEBUG message" << "--")
	LOG_INFO("INFO message" << "--")
	{
		LOG_NAMED_SCOPE("this.is.Logger")
		LOG_INFO("Scoped log" << "info" << "--")
	}
	LOG_WARNING("WARNING message" << "--")
	LOG_ERROR("ERROR message" << "--")
	LOG_FATAL("FATAL message" << "--")
	LOG_WITH_TAG(debug, "important!", "Tag message" << "--")
}
